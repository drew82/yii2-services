<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "service".
 *
 * @property int $id
 * @property string $title
 * @property string $code
 * @property float $price
 * @property string|null $description
 * @property string $status
 * @property string $dt_till
 * @property string $city
 *
 * @property Change[] $changes
 */
class Service extends \yii\db\ActiveRecord
{
    const STATUS_ENABLED = 'enabled';
    const STATUS_DISABLED = 'disabled';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'code', 'price', 'status', 'dt_till', 'city'], 'required'],
            [['price'], 'number'],
            [['description'], 'string'],
            [['dt_till'], 'date', 'format' => 'php:Y-m-d'],
            [['title', 'city'], 'string', 'max' => 100],
            [['code'], 'string', 'max' => 16],
            [['code'], 'unique'],
            [['title'], 'unique'],
            [['status'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'code' => Yii::t('app', 'Code'),
            'price' => Yii::t('app', 'Price'),
            'description' => Yii::t('app', 'Description'),
            'status' => Yii::t('app', 'Status'),
            'dt_till' => Yii::t('app', 'Dt Till'),
            'city' => Yii::t('app', 'City'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return ServiceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ServiceQuery(get_called_class());
    }

    /**
     * Gets query for [[Changes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChanges()
    {
        return $this->hasMany(Change::className(), ['service_id' => 'id']);
    }

    public static function listAll($outputField = 'code'): array
    {
        $query = self::find()->orderBy($outputField);

        return \yii\helpers\ArrayHelper::map($query->all(), 'id', $outputField);
    }
}
