<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Service]].
 *
 * @see Service
 */
class ServiceQuery extends \yii\db\ActiveQuery
{

    /**
     * Add status="enabled" condition
     * @return $this
     */
    public function isEnabled()
    {
        return $this->andWhere(['status' => Service::STATUS_ENABLED]);
    }

    /**
     * Add city condition
     * @param string $cityname
     * @return $this
     */
    public function inCity(string $cityname)
    {
        return $this->andWhere(['city' => $cityname]);
    }
 
    /**
     * {@inheritdoc}
     * @return Service[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Service|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
