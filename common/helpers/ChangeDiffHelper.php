<?php

namespace common\helpers;

class ChangeDiffHelper
{
    protected array $changes;

    protected function __construct(string $json)
    {
        $this->changes = json_decode($json, true);
    }

    public static function build(string $json)
    {
        return new self($json);
    }

    public function show(): string
    {
        $ret = [];

        foreach ($this->changes as $change) {
            $ret[] = sprintf('%s: "%s" -> "%s"',
                    $change['attribute'], $change['old_value'], $change['new_value']);
        }

        return implode('<br />', $ret);
    }
}
