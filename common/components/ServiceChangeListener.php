<?php

namespace common\components;

use yii\base\BootstrapInterface;
use yii\base\Event;
use common\models\Service;
use common\models\Change;

class ServiceChangeListener implements BootstrapInterface
{

    public function bootstrap($app)
    {
        Event::on(Service::class, Service::EVENT_BEFORE_UPDATE, [$this, 'serviceUpdate']);
    }

    public function serviceUpdate(Event $event): void
    {
        /** @var Service $service */
        $service = $event->sender;

        if ($service->getDirtyAttributes()) {
            $changes = $this->getChangedAttributesAndTheirValues($service);
            $this->saveServiceAttributesChanges($service->id, $changes);
        }
    }

    protected function getChangedAttributesAndTheirValues(Service $service): array
    {
        $changes = [];
        $oldAttributes = $service->getOldAttributes();

        foreach ($service->getDirtyAttributes() as $attribute => $oldValue) {
            $changes[] = [
                'attribute' => $attribute,
                'old_value' => $oldAttributes[$attribute],
                'new_value' => $service->{$attribute},
            ];
        }

        return $changes;
    }

    protected function saveServiceAttributesChanges(int $serviceId, array $changes): void
    {
        $change = new Change();
        $change->user_id = \Yii::$app->user->getId();
        $change->service_id = $serviceId;
        $change->dt = date('Y-m-d H:i:s');
        $change->diff = json_encode($changes);

        if (!$change->save()) {
            $msg = var_export($change->getErrorSummary(false), true);
            throw new \yii\db\Exception($msg);
        }
    }

}
