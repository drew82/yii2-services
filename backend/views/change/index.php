<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ChangeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Changes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="change-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Change'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//            'user_id',
            [
                'attribute' => 'user_id',
                'value' => 'user.username',
                'filter' => \common\models\User::listAll(),
                'label' => Yii::t('app', 'User'),
            ],
//            'service_id',
            [
                'attribute' => 'service_id',
                'value' => 'service.code',
                'filter' => \common\models\Service::listAll(),
                'label' => Yii::t('app', 'Service'),
            ],
            'dt',
//            'diff',
            [
                'attribute' => 'diff',
                'value' => function ($data) {
                    return \common\helpers\ChangeDiffHelper::build($data->diff)->show();
                },
                'format' => 'html',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
