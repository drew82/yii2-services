<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Service */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="service-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-9">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-3">
            <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-3">
            <?= $form->field($model, 'price')->textInput() ?>
        </div>

        <div class="col-3">
            <?= $form->field($model, 'dt_till')->textInput() ?>
        </div>

        <div class="col-2">
            <?= $form->field($model, 'status')->dropDownList([
                \common\models\Service::STATUS_DISABLED => Yii::t('app', 'Disabled'),
                \common\models\Service::STATUS_ENABLED => Yii::t('app', 'Enabled'),                
            ], ['disabled' => !Yii::$app->user->can('backend:service:status')]) ?>
        </div>

        <div class="col-4">
            <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
