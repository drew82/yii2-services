<?php

namespace backend\models;

use Yii;

class Service extends \common\models\Service
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['status', 'validateStatusChangeAccess', 'when' => function ($model, $attribute) {
                    /** @var Service $model */
                    return in_array($attribute, array_keys($model->getDirtyAttributes()));
            }],
        ]);
    }

    public function validateStatusChangeAccess($attribute, $params, $validator)
    {
        if (!Yii::$app->user->can('backend:service:status')) {
            $this->addError($attribute, Yii::t('app', 'You cannot modify service status'));
        }
    }
}
