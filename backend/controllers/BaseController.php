<?php

namespace backend\controllers;

abstract class BaseController extends \yii\web\Controller
{
    /**
     * Check access
     * @param \yii\base\Action $action
     * @return boolean
     */
    public function beforeAction($action): bool
    {
        $permission = sprintf('backend:%s:%s', $action->controller->id, $action->id);

        if (\Yii::$app->user->can($permission)) {
            return parent::beforeAction($action);
        } else {
            throw new \yii\web\ForbiddenHttpException();
        }
    }
}
