<?php

use yii\db\Migration;
use common\models\User;

/**
 * Class m210916_145852_seed_user_table
 */
class m210916_145852_seed_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $faker = Faker\Factory::create();

        foreach (range(0, 9) as $i) {
            $user = new User();
            $user->username = $faker->userName;
            $user->auth_key = \Yii::$app->security->generateRandomString();
            $user->password_hash = \Yii::$app->security->generatePasswordHash('123');
            $user->email = $faker->email;
            $user->status = User::STATUS_ACTIVE;
            $user->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210916_145852_seed_user_table cannot be reverted.\n";

        return false;
    }
    */
}
