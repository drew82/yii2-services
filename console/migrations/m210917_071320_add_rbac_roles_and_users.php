<?php

use yii\db\Migration;
use common\models\User;

/**
 * Class m210917_071320_add_rbac_roles_and_users
 */
class m210917_071320_add_rbac_roles_and_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $rbac = \Yii::$app->authManager;

        $serviceIndex = $rbac->createPermission('backend:service:index');
        $rbac->add($serviceIndex);

        $serviceCreate = $rbac->createPermission('backend:service:create');
        $rbac->add($serviceCreate);

        $serviceStatus = $rbac->createPermission('backend:service:status');
        $rbac->add($serviceStatus);

        $serviceUpdate = $rbac->createPermission('backend:service:update');
        $rbac->add($serviceUpdate);

        $changeIndex = $rbac->createPermission('backend:change:index');
        $rbac->add($changeIndex);

        $roleOperator = $rbac->createRole(User::ROLE_OPERATOR);
        $rbac->add($roleOperator);
        $rbac->addChild($roleOperator, $serviceIndex);
        $rbac->addChild($roleOperator, $serviceUpdate);
        $rbac->addChild($roleOperator, $changeIndex);

        $roleAdmin = $rbac->createRole(User::ROLE_ADMIN);
        $rbac->add($roleAdmin);
        $rbac->addChild($roleAdmin, $serviceCreate);
        $rbac->addChild($roleAdmin, $serviceStatus);

        // admin includes operator
        $rbac->addChild($roleAdmin, $roleOperator);

        // assign roles to users
        foreach (User::find()->all() as $user) {
            /** @var User $user */
            $role = 'role' . ucfirst($user->getRole());
            $rbac->assign(${$role}, $user->id);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $rbac = \Yii::$app->authManager;
        $rbac->removeAll();
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210917_071320_add_rbac_roles_and_users cannot be reverted.\n";

        return false;
    }
    */
}
