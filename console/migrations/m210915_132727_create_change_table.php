<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%change}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%service}}`
 */
class m210915_132727_create_change_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%change}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'service_id' => $this->integer()->notNull(),
            'dt' => $this->timestamp()->notNull(),
            'diff' => $this->json()->notNull(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-change-user_id}}',
            '{{%change}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-change-user_id}}',
            '{{%change}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `service_id`
        $this->createIndex(
            '{{%idx-change-service_id}}',
            '{{%change}}',
            'service_id'
        );

        // add foreign key for table `{{%service}}`
        $this->addForeignKey(
            '{{%fk-change-service_id}}',
            '{{%change}}',
            'service_id',
            '{{%service}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-change-user_id}}',
            '{{%change}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-change-user_id}}',
            '{{%change}}'
        );

        // drops foreign key for table `{{%service}}`
        $this->dropForeignKey(
            '{{%fk-change-service_id}}',
            '{{%change}}'
        );

        // drops index for column `service_id`
        $this->dropIndex(
            '{{%idx-change-service_id}}',
            '{{%change}}'
        );

        $this->dropTable('{{%change}}');
    }
}
