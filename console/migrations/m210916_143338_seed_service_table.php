<?php

use yii\db\Migration;
use common\models\Service;

/**
 * Class m210916_143338_seed_service_table
 */
class m210916_143338_seed_service_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $faker = Faker\Factory::create();

        foreach (range(0, 100) as $i) {
            $service = new Service();
            $service->title = $faker->sentence;
            $service->code = $faker->swiftBicNumber;
            $service->price = $faker->randomFloat(2, 1000, 20000);
            $service->dt_till = $faker->date;
            $service->status = mt_rand(1, 2) % 2 == 0 ? Service::STATUS_ENABLED : Service::STATUS_DISABLED;
            $service->city = $faker->city;
            $service->description = $faker->paragraph;
            $service->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210916_143338_seed_service_table cannot be reverted.\n";

        return false;
    }
    */
}
