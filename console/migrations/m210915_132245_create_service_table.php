<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%service}}`.
 */
class m210915_132245_create_service_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%service}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(100)->notNull()->unique(),
            'code' => $this->string(16)->notNull()->unique(),
            'price' => $this->decimal()->notNull(),
            'description' => $this->text(),
            'is_active' => $this->boolean()->notNull(),
            'dt_till' => $this->date()->notNull(),
            'city' => $this->string(100)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%service}}');
    }
}
