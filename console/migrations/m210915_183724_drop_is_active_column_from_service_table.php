<?php

use yii\db\Migration;

/**
 * Handles dropping columns from table `{{%service}}`.
 */
class m210915_183724_drop_is_active_column_from_service_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%service}}', 'is_active');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%service}}', 'is_active', $this->boolean()->notNull());
    }
}
