<?php

use yii\db\Migration;
use common\models\User;

/**
 * Class m210917_092425_add_rbac_permission
 */
class m210917_092425_add_rbac_permission extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $rbac = \Yii::$app->authManager;

        $serviceView = $rbac->createPermission('backend:service:view');
        $rbac->add($serviceView);

        $changeView = $rbac->createPermission('backend:change:view');
        $rbac->add($changeView);

        $roleOperator = $rbac->getRole(User::ROLE_OPERATOR);

        $rbac->addChild($roleOperator, $serviceView);
        $rbac->addChild($roleOperator, $changeView);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210917_092425_add_rbac_permission cannot be reverted.\n";

        return false;
    }
    */
}
