<?php

namespace api\controllers;

use Yii;
use common\models\Service;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;

class ServiceController extends Controller
{
//    public $modelClass = 'common\models\Service';

    /**
     * Lists all Service models
     * @return ActiveDataProvider
     */
    public function actionIndex()
    {
        $query = Service::find();

        $cityname = Yii::$app->request->get('city');
        if ($cityname) {
            $query->inCity($cityname);
        }

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }

    /**
     * Displays a single Service model.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->findModel($id);
    }

    /**
     * Deletes an existing Service model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
    }

    /**
     * Finds the Service model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Service the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Service::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested service does not exist.'));
    }
}
