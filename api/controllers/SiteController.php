<?php

namespace api\controllers;

use Yii;
use yii\rest\Controller;

class SiteController extends Controller
{
    public function actionError()
    {
        return ['error' => Yii::t('app', 'The requested method does not exist.')];
    }
}
