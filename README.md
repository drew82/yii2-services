# Тестовое задание

### Необходимо разработать на Yii2 минисервис по управлению списком услуг с сохранением истории изменений. 
#### Он должен состоять из двух частей: админ-панель (для CRUD) и простого RESTful API (для получения информации об услугах). 

Услуга (Service) имеет следующие атрибуты:  
1. Название  
2. Код  
3. Цена  
4. Описание  
5. Статус (включена/выключена)  
6. Срок действия  
7. Город действия  

В админ-панель имеют доступ пользователи, у которых одна из двух ролей: **администратор** или **оператор** (интерфейс управления пользователями реализовывать не нужно).  
  
Пользователи могут совершать следующие действия:  
1. Создание услуги (только администраторы)  
2. Включение/выключение услуги (только администраторы)  
3. Редактирование остальных параметров (все роли)  
4. Просмотр истории изменений (все роли)  
  
История изменений должна давать информацию:  
1. Какие атрибуты изменены (предыдущее значение)  
2. Дата и время изменения  
3. Пользователь, внесший изменения  
  
В API должно быть как минимум 2 метода:  
1. Получение списка услуг в указанном городе  
2. Просмотр информации о конкретной услуге по ее id  

Необходимо задействовать встроенные во фреймворк механизмы миграций и RBAC.  
Должен использоваться шаблон приложения advanced. Реализацию недостающих и неуказанных сущностей (например User) делайте по своему усмотрению.  
БД можно использовать любую. API делать с помощью RestController.  
  
  
## Как развернуть приложение
```
git clone git@bitbucket.org:drew82/yii2-services.git .
./init
$EDITOR ./common/config/main-local.php     # прописать dsn, username, password
composer install
./yii migrate
```
